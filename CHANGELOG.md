Change Log
============

1.0.5
-----

* Renamed NOSA to NASA in package description
* Removed installing CI scripts in CI

1.0.4
-----

* Remove trusty CI

1.0.3
-----

* Added NOSA license file

1.0.2
-----

* Added xenial CI
