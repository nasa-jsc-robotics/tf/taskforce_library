import logging
import hashlib
import os
import sys
import shutil
import types

from taskforce_common.utils import reload_module_by_name

from utils import calculateHash, getLibraryInfo

logger = logging.getLogger('taskforce_library')

DEFAULT_LIBRARY_FOLDER_NAME = '.taskforce_local_library'


class LibraryManagerException(Exception):
    pass


class LibraryManager(object):
    """
    The purpose of the LibraryManager is to organize Python code into packages and
    modules.  Packages can be added and removed from the library to preserve state
    between instantiations.  By using the 'addLibraryToPath' method, other applications
    can directly '__import__' library modules at run-time.  Code stored in the library
    can be created and destroyed at any time, so care must be taken.
    """
    def __init__(self, library_path=None):
        """
        @type library_path string
        @param library_path Root folder of the library, or location of existing library
        """
        if library_path == None:
            # set library_path to user's home folder.
            # if there is no HOME, environment variable, set it to './'
            library_path = os.path.join(os.environ.get('HOME','./'),DEFAULT_LIBRARY_FOLDER_NAME)

        self.logger = logger.getChild('LibraryManager')
        self.library_path = library_path
        if not os.path.exists(self.library_path):
            self.logger.debug('Creating library folder:"{}"'.format(self.library_path))
            os.makedirs(self.library_path)

    def _addInitDotPyFiles(self):
        """
        Populate the library tree with __init__.py files
        """
        for root, dirs, files in os.walk(self.library_path):
            if '__init__.py' not in files:
                filename = os.path.join(root,'__init__.py')
                with open(filename, 'w') as f_ptr:
                    f_ptr.write('')
                f_ptr.close()

    def addLibraryToPath(self):
        """
        Update the sys.path to include this library
        """
        path = sys.path
        if self.library_path not in path:
            self.logger.debug('Adding path:"{}" to sys.path'.format(self.library_path))
            sys.path = [self.library_path] + sys.path

    def addModule(self, module_name, source_code, force_reload=False):
        """
        Add a module to the library

        @type module_name string
        @param module_name Name of the module to add.  This should be "fully qualified" if it is a sub-package
            i.e. : packagename.MyModule

        @type source_code string
        @param source_code Contents of the module
        """
        logger.debug('Adding task: module_name="{}"'.format(module_name))

        module_path = os.path.join(self.library_path, *module_name.split('.')) + '.py'
        module_folder = os.path.dirname(module_path)

        if not os.path.exists(module_folder):
            logger.debug('Creating module folder:"{}"'.format(module_folder))
            os.makedirs(module_folder)

        with open(module_path, 'w') as f:
            logger.debug('writing file:"{}"'.format(module_path))
            f.write(source_code)
            f.close()

        for root, dirs, files in os.walk(self.library_path):
            for dir in dirs:
                init_dot_py_path = os.path.join(root,dir,'__init__.py')
                if not os.path.exists(init_dot_py_path):
                    with open(init_dot_py_path,'w') as f_ptr:
                        f_ptr.write('')
                    f_ptr.close()

        self._addInitDotPyFiles()
        if force_reload:
            self.reloadModule(module_name)

    def clearLibrary(self):
        """
        Remove ALL files from the library
        """
        if os.path.exists(self.library_path):
            self.logger.debug('Removing library:"{}"'.format(self.library_path))
            shutil.rmtree(self.library_path)
        os.mkdir(self.library_path)

    def getLibraryInfo(self):
        """
        Get info about the local library
        """
        return getLibraryInfo(self.library_path,followlinks=True)

    def getModulePath(self, module_name):
        """
        Return the filepath of a module
        """
        info = self.getModuleInfo(module_name)
        return info['path']

    def getModuleHash(self, module_name):
        """
        Return the hash of a module
        """
        info = self.getModuleInfo(module_name)
        return info['hash']

    def getModuleInfo(self, module_name):
        """
        Get info about a module in the library
        """
        info = self.getLibraryInfo()
        try:
            module_info = info[module_name]
        except KeyError as e:
            raise LibraryManagerException('Library module "{}" does not exist'.format(module_name))
        return module_info

    def reloadModule(self, module_name):
        reload_module_by_name(module_name)

    def removeModule(self, module_name):
        """
        Remove a module from the library
        """
        file_path = self.getModulePath(module_name)
        if os.path.exists(file_path):
            os.remove(file_path)

    def removePackage(self, package_name):
        """
        Remove a package from the library
        """
        package_path = os.path.join(self.library_path,package_name)
        if os.path.exists(package_path):
            shutil.rmtree(package_path)
        else:
            raise LibraryManagerException('Trying to remove package "{}", but that package does not exist')

    def removeLibraryFromPath(self):
        """
        Remove this library from sys.path
        """
        path = sys.path
        if self.library_path in path:
            self.logger.debug('Removing path:"{}" from sys.path'.format(self.library_path))
            path = [p for p in path if p != self.library_path]
        sys.path = path

