import hashlib
import os

def calculateHash(filename):
    """
    Calculate the hash value for a file
    """
    with open(filename,'r') as f:
        contents = f.read()
        f.close()

    return hashlib.sha1(contents).hexdigest()

def getLibraryInfo(library_path, followlinks=False):
    """
    Get info about the local library
    """
    library_info = {}
    for root, dirs, files in os.walk(library_path, followlinks=followlinks):
        if '__init__.py' in files:
            package_name = os.path.relpath(root, library_path).replace('/', '.')
            modules = []
            module_file_names = [f for f in files if f.endswith('.py') and f != "__init__.py"]
            for module_file_name in module_file_names:
                module_name = '.'.join([package_name, module_file_name.split('.py')[0]]).strip('.')
                module_abs_path = os.path.join(root, module_file_name)
                module_rel_path = os.path.relpath(module_abs_path, library_path)
                module_hash = calculateHash(module_abs_path)
                module_info = {
                    'hash': module_hash,
                    'path': module_abs_path,
                    'rel_path': module_rel_path,
                }
                library_info[module_name] = module_info
    return library_info