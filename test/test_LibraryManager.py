import unittest

import importlib
import os, sys
import shutil

from taskforce_library import LibraryManager, LibraryManagerException

hello_world_source_code = """
def hello_world(name):
    print 'Hello, {}!'.format(name)
"""

MyClass_source_code = """
class MyClass(object):
    def __init__(self, val):
        self.val = val

    def getVal(self):
        return self.val
"""

MySubClass_source_code = """
from my_package import MyClass

class MySubClass(MyClass.MyClass):
    def setVal(self, val):
        self.val = val
"""

TestClass_source_code_v1 = """class TestClass(object):
    def __init__(self, val, kw='keyword'):
        self.val = val
        self.other = 'green'
        self.kw = kw

    def getVal(self):
        return self.val

    def getKw(self):
        return self.kw
"""

TestClass_source_code_v2 = """class TestClass(object):
    def __init__(self, val, kw='keyword'):
        self.val = val
        self.other = 'blue'
        self.kw = kw

    def getVal(self):
        return self.val

    def getKw(self):
        return self.kw

"""


TEST_FILE_PATH = os.path.realpath(__file__)
TEST_FOLDER = os.path.dirname(TEST_FILE_PATH)

class TestLibraryManager(unittest.TestCase):

    def setUp(self):
        self.library_path = os.path.join(TEST_FOLDER,'.taskforce_test_library')

    def tearDown(self):
        if os.path.exists(self.library_path):
            shutil.rmtree(self.library_path)

    #@unittest.skip('reason')
    def test_constructor(self):
        manager = LibraryManager(self.library_path)
        self.assertTrue(self.library_path)

    #@unittest.skip('reason')
    def test_constructor_path_exists(self):
        if not os.path.exists(self.library_path):
            os.makedirs(self.library_path)
        manager = LibraryManager(self.library_path)
        self.assertTrue(os.path.exists(self.library_path))

    #@unittest.skip('reason')
    def test_addModule(self):
        manager = LibraryManager(self.library_path)
        manager.addModule('my_package.hello_world', hello_world_source_code)

        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','__init__.py')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','hello_world.py')))

        manager.addModule('my_package.MyClass', MyClass_source_code)

        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','__init__.py')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','MyClass.py')))

    #@unittest.skip('reason')
    def test_addModule_nestedPackage(self):
        manager = LibraryManager(self.library_path)
        manager.addModule('my_package.sub_package.hello_world', hello_world_source_code)

        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','__init__.py')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','sub_package')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','sub_package','hello_world.py')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','sub_package','__init__.py')))

    #@unittest.skip('reason')
    def test_reloadModule(self):
        manager = LibraryManager(self.library_path)
        manager.addLibraryToPath()

        val = 1.234
        kw  = True

        # force a reload the first time, since we may have existing code from previous tests
        manager.addModule('TestClass', TestClass_source_code_v1,force_reload=False)

        try:
            module = importlib.import_module('TestClass')
        except ImportError as e:
            for p in sys.path:
                print p
            raise e

        class_def = getattr(module, 'TestClass')
        obj = class_def(val, kw=kw)

        # baseline assert that we created a version 1 of the object
        self.assertEqual('green', obj.other)

        # change the source, do NOT force a reload, and verify that when we create a new object it is still the old version
        manager.addModule('TestClass', TestClass_source_code_v2, force_reload=True)
        module = importlib.import_module('TestClass')
        class_def = getattr(module, 'TestClass')
        obj = class_def(val, kw=kw)
        self.assertEqual('blue', obj.other)

        # now, force a reload, recreate the object, and verify that we have a version 2 of the object
        manager.reloadModule('TestClass')
        module = importlib.import_module('TestClass')

        class_def = getattr(module, 'TestClass')
        obj = class_def(val, kw=kw)
        self.assertEqual('blue', obj.other)

    #@unittest.skip('reason')
    def test_removeModule(self):
        manager = LibraryManager(self.library_path)

        manager.addModule('my_package.hello_world', hello_world_source_code)
        manager.addModule('my_package.MyClass', MyClass_source_code)

        #should raise
        self.assertRaises(LibraryManagerException, manager.removeModule, 'bogus_package.bogus_module')
        manager.removeModule('my_package.hello_world')
        self.assertFalse(os.path.exists(os.path.join(self.library_path,'my_package','hello_world.py')))

        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','__init__.py')))
        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','MyClass.py')))

    #@unittest.skip('reason')
    def test_removePackage(self):
        manager = LibraryManager(self.library_path)

        manager.addModule('my_package.hello_world', hello_world_source_code)
        manager.addModule('my_package.MyClass', MyClass_source_code)

        self.assertTrue(os.path.exists(os.path.join(self.library_path,'my_package','MyClass.py')))
        self.assertRaises(LibraryManagerException,manager.removePackage,'bogus_package')

        manager.removePackage('my_package')
        self.assertFalse(os.path.exists(os.path.join(self.library_path,'my_package')))
        self.assertFalse(os.path.exists(os.path.join(self.library_path,'my_package','MyClass.py')))


    #@unittest.skip('reason')
    def test_addLibraryToPath(self):
        manager = LibraryManager(self.library_path)

        manager.addModule('my_package.hello_world', hello_world_source_code)
        manager.addModule('my_package.MyClass', MyClass_source_code)

        manager.addLibraryToPath()
        self.assertTrue(os.path.join(self.library_path,self.library_path) in sys.path)

        module = importlib.import_module('my_package.MyClass')
        self.assertTrue(hasattr(module,'MyClass'))

        class_def = getattr(module, 'MyClass')
        val = 5
        class_instance = class_def(val)
        self.assertEqual(class_instance.getVal(), val)

    #@unittest.skip('reason')
    def test_addLibraryToPath_dependencies(self):
        manager = LibraryManager(self.library_path)

        manager.addModule('my_package.MyClass', MyClass_source_code)
        manager.addModule('other_package.MySubClass', MySubClass_source_code)

        module = importlib.import_module('other_package.MySubClass')
        class_def = getattr(module, 'MySubClass')

        class_instance = class_def(5)
        self.assertEqual(class_instance.getVal(), 5)

        class_instance.setVal(10)
        self.assertEqual(class_instance.getVal(), 10)

    #@unittest.skip('reason')
    def test_removeLibraryFromPath(self):
        manager = LibraryManager(self.library_path)

        manager.addLibraryToPath()
        self.assertTrue(os.path.join(self.library_path,self.library_path) in sys.path)

        manager.removeLibraryFromPath()
        self.assertFalse(os.path.join(self.library_path,self.library_path) in sys.path)

    #@unittest.skip('reason')
    def test_clearLibrary(self):
        manager = LibraryManager(self.library_path)

        manager.addModule('my_package.MyClass', MyClass_source_code)
        manager.addModule('other_package.MySubClass', MySubClass_source_code)

        manager.clearLibrary()
        self.assertFalse(os.path.exists(os.path.join(self.library_path,'my_package')))
        self.assertFalse(os.path.exists(os.path.join(self.library_path,'my_package','__init__.py')))
        self.assertFalse(os.path.exists(os.path.join(self.library_path,'my_package','MyClass.py')))

    #@unittest.skip('reason')
    def test_getModuleHash(self):
        manager = LibraryManager(self.library_path)

        manager.addModule('my_package.MyClass', MyClass_source_code)
        h = manager.getModuleHash('my_package.MyClass')

        self.assertEqual(h, '9cbcb34a2d8dcc632ec7e13dafed55abfab9eccb')

    #@unittest.skip('reason')
    def test_getModuleInfo(self):
        manager = LibraryManager(self.library_path)
        manager.addModule('my_package.MyClass', MyClass_source_code)
        manager.addModule('other_package.MySubClass', MySubClass_source_code)
        manager.addModule('my_package.sub_package.hello_world', hello_world_source_code)

        manager.getModuleInfo('my_package.sub_package.hello_world')

        from pprint import pprint
        print ('\n')
        pprint(manager.getLibraryInfo())

if __name__ == "__main__":
    unittest.main()